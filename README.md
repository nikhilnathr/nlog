# NLOG

  

This is a utility used to login/logout of the NITC(National Institute of Technology Calicut) network. Insert a list of logins to this script and your the constant irritation of typing your credentials again and again will be no more. You can load a list of logins to this script and login at random from that list. Furthermore, if any login credential in the list was changed the script will automatically delete it from the list. 

  

## Requirements

  

PHP Version 7 or above is recomended as I have only tried in those versions.

>  **Note:** No other requirements was found. It doesn't mean that there aren't any. If you find any changes or have any improvements feel free to submit a merge request.

  

Installing PHP is a tedious task in most systems because most systems does not support PHP out of the box, So here are some information on how to install PHP

  

## Installation

  

## UNIX Systems

  

Read the official installation guidelines by PHP https://www.php.net/manual/en/install.unix.php.

  

But in most cases its just a single line command to install PHP. So please make sure to search your distribution's package manager. I'll list down some of the package managers I found PHP on.

  

#### Ubuntu/Debian

```
sudo apt install php
```

  

#### Red Hat/Fedora/CentOS

```
sudo yum install php
```

  

#### Arch Linux/Manjaro

```
sudo pacman -S php
```

  

### Windows

  

The resourse pointed by the official PHP website is https://www.php.net/manual/en/install.windows.php

  

But the only way I have tried earlier is through a XAMPP server. Go to their [website](https://www.apachefriends.org/download.html]) and download the latest version and install it. You may have to add the `php.exe` path to the **ENVIRONMENT_VARIABLE** to make it run from command_prompt

  

### MacOS

  

Read the official website's installation steps https://www.php.net/manual/en/install.macosx.php

>  **Note:** These are just my suggestion. Please do what you are most comfortable with.

  
  

## Usage

  

Clone the project into a directory. Open terminal/command prompt/powershell in that directory.

> **Advanced:** You may also give the file executable permission `chmod +x nlog` and run the following use cases without typing the `php` always. But be careful about the shabang as I have placed the default one and your system might be different. So, change it accordingly.

### Login with your credentials

```
php nlog -u=username -p=password
```

  

### Load some credentials from a file

  

Make a list of available login details into a text file which is of the form

  

```
username1:password1

username2:password2

.....

.....
```

  

Check the sample.txt for some examples of the same. Then to load/append the credentials into your script.

  

```
php nlog -a=data_file
```

  

### Login with random credentials

  

```
php nlog -x
```

### Login with keepalive loop

```
php nlog -xk
```

  

### Logout

```
php nlog -l
```

  ## Useful

>  **Psst...for linux users**: `nohup php nlog.php -x > /dev/null &2>1 &`
